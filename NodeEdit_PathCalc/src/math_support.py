from math import sqrt,pow
def distance(p1,p2):
    dx=p1[0]-p2[0]
    dy=p1[1]-p2[1]
    return sqrt(pow(dx,2)+pow(dy,2))
def inBetween(p1,p2):
    dx=abs(p1[0]-p2[0])
    dy=abs(p1[1]-p2[1])
    mx=min(p1[0],p2[0])+dx/2
    my=min(p1[1],p2[1])+dy/2
    return [mx,my]
