from math_support import distance
import pygame
import pygame.font
class World:
    def __init__(self,pointer):
        self.nodes=[]
        self.connections=[]
        self.buttons=[]
        self.solution=[]
        self.state='add_and_move'
        self.states=['add_and_move','connect','set_journey']
        self.font = pygame.font.Font("freesansbold.ttf", 20)
        self.journey=None
    def hittest(self,pos):
        for button in self.buttons:
            if(pos[0]<button.downright[0] and pos[1]<button.downright[1] and
               pos[0]>button.topleft[0] and pos[1]>button.topleft[1]):
                return button
        try:
            for node in self.nodes:
                if(distance(node.pos,pos)<node.radius):
                    #hit objects are always brought to front
                    self.bring_to_front(node)
                    return node
        except TypeError:
            print "No nodes in storage"
        return None
    def bring_to_front(self,hitobject):
        if hitobject in self.nodes:
            self.nodes.remove(hitobject)
            self.nodes.append(hitobject)
        