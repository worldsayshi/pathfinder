from math_support import distance
class Pointer:
    def buttonDown(self,world,event,pos):
        hitobject=world.hittest(pos)
        try:
            if(hitobject.isbutton==True):
                hitobject.click()
                return
        except AttributeError:
            pass
        if event.dict.get('button') == 1:
            #create or drag
            if(hitobject==None and world.state=='add_and_move'):
                hitobject=Node(pos)
                world.nodes.append(hitobject)
            if(world.state=='add_and_move'):
                self.dragObject=hitobject
            if(world.state=='connect'):
                if(hitobject!=None):
                    self.dragObject=Connection(hitobject,self)
                    world.connections.append(self.dragObject)
            if(world.state=='set_journey'):
                if(hitobject!=None):
                    self.dragObject=Connection(hitobject,self)
                    world.journey=self.dragObject
    def move(self,world,pos):
        self.pos=pos
        hitobject=world.hittest(pos)
        try:
            if(hitobject.isbutton==True):
                return
        except AttributeError:
            pass
        if(world.state=='add_and_move'):
            try:
                world.solution=[]
                self.dragObject.pos=pos
            except:
                pass
    def buttonUp(self,world,event,pos):
        if(world.state=='add_and_move'):
            self.dragObject=None
        if(world.state=='connect' or world.state=='set_journey'):
            hitobject=world.hittest(pos)
            if(hitobject==None):
                world.journey=None
                try:
                    world.connections.remove(self.dragObject)
                except (ValueError, AttributeError):
                    pass
            else:
                try:
                    if(hitobject.isbutton==True):
                        return
                except AttributeError:
                    pass
                self.dragObject.B=hitobject
                self.dragOject=None
nodecount=1
class Node:
    def __init__(self,pos):
        global nodecount
        self.pos=pos
        self.name="N"+str(nodecount)
        nodecount+=1
        self.radius=12
        self.color=(255,255,255)
        self.width=5
class Connection:
    def __init__(self,A,B):
        self.A=A
        self.B=B
        self.color=(255,255,255)
        self.width=2
        self.value=0
    def updateValue(self):
        self.value=int(distance(self.A.pos,self.B.pos))
class Button:
    def __init__(self,world,text,topleft,downright):
        self.text=text
        self.isbutton=True
        world.buttons.append(self)
        self.topleft=topleft
        self.downright=downright
    def click(self):
        #print self.text+" clicked"
        self.clicked=True
    def poll(self):
        try:
            if(self.clicked):
                self.clicked=False
                return True
            else:
                return False
        except AttributeError:
            return False
