from math_support import inBetween, distance
from particles import Pointer, Button
from pygame.locals import QUIT, K_ESCAPE, MOUSEBUTTONDOWN, MOUSEBUTTONUP, \
    MOUSEMOTION, K_u
from world import World
import os
import pygame
import sys
if sys.platform == 'win32' or sys.platform == 'win64':
    os.environ['SDL_VIDEO_CENTERED'] = '1'
def GetInput():
    global pointer,world
    key = pygame.key.get_pressed()
    mousepos=pygame.mouse.get_pos()
    events=pygame.event.get()
    for event in events:
        if event.type == QUIT or key[K_ESCAPE]:
            pygame.quit(); sys.exit()
        if event.type == MOUSEBUTTONDOWN:
            pointer.buttonDown(world,event,mousepos)
        if event.type == MOUSEBUTTONUP:
            pointer.buttonUp(world,event,mousepos)
        if event.type == MOUSEMOTION:
            pointer.move(world,mousepos)
            for connection in world.connections:
                connection.updateValue()
            try:
                world.journey.updateValue()
            except AttributeError:
                pass
    for button in world.buttons:
        if button.poll():
            if(button.text=="calc"):
                RunCalculations()
                break
            world.state=button.text
            print "State set to "+button.text
def RunCalculations():
    print "Opening data documents"
    CONN = open("..\\..\\common\\neighbours.txt","w")
    DIST = open("..\\..\\common\\distances.txt","w")
    CONF = open("..\\..\\common\\config.txt","w")
    
    #Write journey to config file
    try:
        CONF.write("start="+world.journey.A.name+"\n"+"goal="+world.journey.B.name+"\n")
    except:
        print "Error: Journey not set!"
        CONF.close()
        return
    CONF.close()
    
    #Print distances to distance file
    #DIST.write(",")
    #for n in world.nodes:
    #    DIST.write(n.name+",")
    #DIST.write("\n")
    for n1 in world.nodes:
        DIST.write(n1.name+",")
        for n2 in world.nodes:
            DIST.write(str(int(distance(n1.pos,n2.pos)))+",")
        DIST.write("\n")
    DIST.close()
    
    #Write routes to connections file
    for c in world.connections:
        CONN.write(c.A.name+","+c.B.name+","+str(c.value)+"\n")
    CONN.close()

    print "Running java search program"
    os.system("cd ..\\..\\ai_assignment1\\bin\\ & java commandLineInterface.Main -cmd")
    SOL=open("..\\..\\common\\solution.txt")
    world.solution=[]
    for line in SOL.readlines():
        for node in world.nodes:
            line=line.strip()
            if(node.name==line):
                world.solution.append(node.pos)
    SOL.close()
    
def Draw():
    global world,surface
    surface.fill((125,125,125))
    for connection in world.connections:
        pygame.draw.line(surface,connection.color,connection.A.pos,connection.B.pos,connection.width)
        text=world.font.render(str(connection.value), 1, (255, 255, 255))
        textpos=inBetween(connection.A.pos,connection.B.pos)
        surface.blit(text, [textpos[0]+5,textpos[1]])
    if(world.journey!=None):
        pygame.draw.circle(surface, (125,255,255), world.journey.A.pos, 16, 1)
        pygame.draw.circle(surface, (125,255,255), world.journey.B.pos, 14)
        pygame.draw.line(surface,(125,255,255),world.journey.A.pos,world.journey.B.pos,5)
        text=world.font.render(str(world.journey.value), 1, (125,255,255))
        textpos=inBetween(world.journey.A.pos,world.journey.B.pos)
        surface.blit(text, [textpos[0]+5,textpos[1]])
    for button in world.buttons:
        pygame.draw.rect(surface, (255, 255, 255), pygame.Rect(button.topleft,[button.downright[0]-button.topleft[0],button.downright[1]-button.topleft[1]]), 2)
        text = world.font.render(button.text, 1, (255, 255, 255))
        surface.blit(text, button.topleft)
    for node in world.nodes:
        nodepos=node.pos
        try:
            if(node!=world.journey.B):
                pygame.draw.circle(surface, (125,125,125), nodepos, node.radius)#move bg color from here
        except AttributeError:
            pygame.draw.circle(surface, (125,125,125), nodepos, node.radius)#move bg color from here
        pygame.draw.circle(surface, node.color, nodepos, node.radius, node.width)
        textpos=[0,0]
        textpos[0]=node.pos[0]+node.radius
        textpos[1]=node.pos[1]-node.radius
        text = world.font.render(node.name, 1, (255, 255, 255))
        surface.blit(text, textpos)
    if(len(world.solution)>1):
        pygame.draw.lines(surface, (255,255,125), False, world.solution, 10)
    pygame.display.update()
def SetupButtons():
    Left=screen[0]-100
    Top=0
    buttons_to_make=world.states+["calc"]
    for i,state in enumerate(buttons_to_make):
        world.buttons.append(Button(world,state,[Left,Top+30*i],[screen[0],Top+30*(i+1)]))
def Setup():
    global world,pointer,screen,surface
    pygame.init()
    pointer=Pointer()
    world=World(pointer)
    surface = pygame.display.set_mode((300,300))#, pygame.FULLSCREEN)
    screen = surface.get_size()
    SetupButtons()
def Mainloop():
    GetInput()  #Input
    #Move()      #Process
    Draw()      #Output
def main():
    Setup()
    while(True):
        Mainloop()
main()