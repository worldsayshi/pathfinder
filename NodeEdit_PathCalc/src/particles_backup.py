from math_support import distance
class Anchor:
    def __init__(self,parent=None,pos=[0,0]):
        self.setPos(pos)#The root should not have a pos?
        if(parent==None):
            self.parent=None
            self.treelevel=0 #=> This is a root point #TODO check in world that there is only one root
        else:
            self.parent=parent
            self.treelevel=parent.treelevel+1
    def getPos(self):
        print self.name 
        if(self.parent.treelevel>=self.treelevel): 
            print "Error: treerecursion in Node " +self.name +"\nand parent "+self.parent.name+ "\nNode tree level: "+str(self.treelevel)+"\nParent tree level: "+str(self.parent.treelevel)
            
        try:
            parent_pos=self.parent.getPos()
        except AttributeError:
            parent_pos=[0,0]
        Xpos=parent_pos[0]+self.pos[0]
        Ypos=parent_pos[1]+self.pos[1]
        result=[Xpos,Ypos]
        return result
    def setPos(self,pos):
        self.pos=[0,0]
        self.pos[0]=pos[0]
        self.pos[1]=pos[1]
    def attach(self,parent,pos):
        self.parent=parent
        self.treelevel=parent.treelevel+1
        self.setPos(pos)
class Pointer(Anchor):
    def __init__(self,parent):
        self.name="Pointer"
        Anchor.__init__(self,parent)#Allows shifting the pointer to different spaces 
    def buttonDown(self,world,event,pos):
        if(world.state=='add_and_move'):
                hitobject=world.hittest(pos)
                if event.dict.get('button') == 1:
                    if(hitobject==None):
                        if(world.rootAnchor==None or not world.rootAnchor):
                            print "Warning: world has no anchor!"
                        hitobject=Node(world.rootAnchor,pos)
                        world.nodes.append(hitobject)
                    self.start_drag(hitobject)
                elif event.dict.get('button') == 3:
                    pass
        if(world.state=='connect'):
            pass
        if(world.state=='set_journey'):
            pass
    def buttonUp(self,world,event,pos):
        hitobject=world.hittest(pos)
        self.stop_drag(self.dragObject,hitobject,pos)
    def start_drag(self,dragobject):
        print "picking up object, from:"
        print dragobject.parent
        dragobject.attach(self,[0,0])
        print "to:"
        print dragobject.parent
        self.dragObject=dragobject
    def dragMove(self):
        pass
    def stop_drag(self,dragobject,destinationanchor,destinationpos):
        print "releasing object, from:"
        print dragobject.parent
        dragobject.attach(destinationanchor,destinationpos)
        print "to:"
        print dragobject.parent
        self.dragObject=None
nodecount=0
class Node(Anchor):
    def __init__(self,parent,pos):
        global nodecount
        self.name="N"+str(nodecount)
        nodecount+=1
        Anchor.__init__(self,parent, pos)
        if(self.parent==None or not self.parent):
            print "Warning: failed to install parent"
        self.radius=10
        self.width=2
        self.color=(255,255,255)
class Connection():#Connection is not an attachable. It holds two attachables 
    def __init__(self,anchor1,anchor2):
        self.update_value()
        self.handles=[Anchor(anchor1),Anchor(anchor2)]
        self.distancetovalueratio=1.0
    def update_heuristic(self):
        self.value=distance(self.anchors[0].pos,self.anchors[1].pos)*self.distancetovalueratio
    def put_anchor(self,new_anchor,index):
        self.anchors[index]=new_anchor
class Button():
    def __init__(self,title,action,data):
        self.title=title
        self.action=action
        self.data=data
#    def attach(self,topleft,downright):
#        self.handles=[Anchor(topleft),Anchor(downright)]