AI1 lab - A*

A* algoritmen kan testas p� tv� s�tt:

1a. K�r gui (run_gui_win7.bat)
1b. klicka ut noder
1c. klicka p� connect
1d. klicka och dra f�rbindelser
1e. klicka p� set_journey
1f. klicka ochh dra f�r att s�tta start och m�l
1e. klicka p� calc

Gui �r testad p� win 7! 
F�r att k�ra plattformsoberoende k�rs n�gon av main filerna i NodeEdit_PathCalc\src i python. Python och pygame kr�vs d�.

2a. �ndra tabeller i common mappen manuellt
2b. Fr�n kommandotolk, g� till ai_assignment1\bin. 
2c. k�r "java commandLineInterface.Main -cmd"

Resultatet presenteras sist i utskrifterna.