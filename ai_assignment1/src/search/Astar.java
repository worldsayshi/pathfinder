package search;
import java.util.ArrayList;

import java.util.Collections;
import java.util.LinkedList;

import commandLineInterface.CityPathProblem;

import dbBuilder.CityDataBase.Connection;
import dbBuilder.CityDataBase.MatchingException;
import dbBuilder.DistanceDataBase.CouldNotFindNodeException;

public class Astar {

	@SuppressWarnings("serial")
	static public class GoalNotFoundException extends Exception{}
	@SuppressWarnings("serial")
	static public class NodeProcessedAgainException extends Exception{}
	
	/*
	 * 1. 
	 */
	
	/*
	static public class Solution {
		String[] quickest_path;
		String[] processed_path;
		public Solution(String[] quickest_path, String[] processedPath) {
			super();
			this.quickest_path = quickest_path;
			processed_path = processedPath;
		}
	}
	*/
	
	static private class Fringe {
		
		// extends LinkedList?
		LinkedList<searchTreeNode> nodes=new LinkedList<searchTreeNode>();
		
		void print(){
			System.out.println("***********************");
			System.out.println("Fringe:");
			System.out.println("Name,	Estimated Rest,	Cost Taken,	value,");
			for (searchTreeNode node:nodes){
				System.out.println(node.name+",	"+node.estimatedRest+",		"+node.costTaken+",		"+node.value());
			}
		}
		searchTreeNode popCheapest(){
			return nodes.removeFirst();
		}
		
		boolean isEmpty(){
			return nodes.isEmpty();
		}
		
		void add(searchTreeNode node){
			int i=0;
			for(;i<nodes.size();i++){
				searchTreeNode compareNode = nodes.get(i);
				if(compareNode.value() > node.value()){
					break;
				}
			}
			nodes.add(i,node);
			//System.out.println("hello");
		}
	}
	
	private static class searchTreeNode{
		
		String name;
		int costTaken;
		searchTreeNode parent;
		private int estimatedRest;
		public searchTreeNode(String name, searchTreeNode parent, int latestLegCost, int estimatedRest) {
			super();
			this.name = name;
			this.estimatedRest = estimatedRest;
			if(parent!=null){
				this.costTaken = parent.costTaken+latestLegCost; 
			}
			else{
				this.costTaken = latestLegCost;
			}
			this.parent = parent;
		}
		public int value(){
			return costTaken + estimatedRest;
		}
	}
	
	private static ArrayList<searchTreeNode> expandNode(CityPathProblem problem, searchTreeNode Node){
		ArrayList<Connection> branches = problem.getCityDb().getNeighbourConnections(Node.name);
		ArrayList<searchTreeNode> childNodes = new ArrayList<searchTreeNode>();
		String childName=null;
		
		// Go through branches/routes going from the node
		for(Connection branch:branches){
			
			// Fetch the name of node on the other end of the branch
			try{
				childName = branch.getOtherEnd(Node.name);
			}
			catch(MatchingException e){
				System.out.println("Error: Could not find connection between "+Node.name + " and " + childName);
			}
			
			// Calculating cost of the individual route as well as the estimated "bird path"-rest
			int routecost= branch.getCost();
			int estimation;
			try {
				estimation= problem.getDistanceDb().getDistance(childName,problem.getGoal());
			} catch (CouldNotFindNodeException e) {
				e.printStackTrace();
				return null;
			}
			
			// Creating a new node in the search tree and adding it to the list of childnodes
			childNodes.add(new searchTreeNode(childName, Node, routecost,estimation));
		}
		return childNodes;
	}
	
	// Entry point
	public static ArrayList<String> findShortestPath(CityPathProblem problem) throws GoalNotFoundException, NodeProcessedAgainException{
		
		Fringe fr = new Fringe();
		ArrayList<String> nodesProcessed=new ArrayList<String>();
		searchTreeNode goal = null;
		//Estimated rest for start: (this is overdoing it, start estimate is not relevant)
		int est;
		try {
			est = problem.getDistanceDb().getDistance(problem.getStart(),problem.getGoal());
		} catch (CouldNotFindNodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		searchTreeNode startNode = new searchTreeNode(problem.getStart(), null, 0, est);//here
		fr.add(startNode);
		searchTreeNode currNode=null;
		
		while(!fr.isEmpty()){
			fr.print(); 
			currNode = fr.popCheapest();
			System.out.println("Picked: "+currNode.name);
			
			// Shouldn't A* work in "all cases without remembering which nodes have been expanded?
			for(String nodename:nodesProcessed){
				if(currNode.name.equals(nodename)){
					continue;
				}
			}
			
			// Check if current node is the goal node
			if(currNode.name.equals(problem.getGoal())){
				goal=currNode;
				break;
			}
			
			// Aquire neighbour nodes 
			ArrayList<searchTreeNode> neighbours = expandNode(problem, currNode);
			
			// Add neighbours to the fringe (in order of heuristical value)
			for (searchTreeNode neighbour:neighbours){
				fr.add(neighbour);
			}
			nodesProcessed.add(currNode.name);
		}
		
		if(goal==null){throw new GoalNotFoundException();}
		
		ArrayList<String> solution = new ArrayList<String>();
		solution.add(currNode.name);
		while(currNode.parent!=null){
			currNode = currNode.parent;
			solution.add(currNode.name);
		}
		Collections.reverse(solution);
		return solution;
		
		/*
		ArrayList<searchTreeNode> neighbours = expandNode(problem, startNode);
		for (searchTreeNode neighbour:neighbours){
			fr.add(neighbour);
		}
		searchTreeNode nextnode = fr.getCheapest();
		*/
	}
}
