package commandLineInterface;

import dbBuilder.CityDataBase;
import dbBuilder.DistanceDataBase;

public class CityPathProblem {
	
	CityDataBase citydb=null;
	DistanceDataBase distdb=null;
	String start=null;
	String goal=null;
	
	public String getStart(){
		return start;
	}
	
	public String getGoal(){
		return goal;
	}
	
	public CityDataBase getCityDb(){
		return citydb;
	}
	
	public DistanceDataBase getDistanceDb(){
		return distdb;
	}
	
	public CityPathProblem(CityDataBase citydb, DistanceDataBase distdb,
			String start, String goal) {
		super();
		this.distdb = distdb;
		this.citydb = citydb;
		this.start = start;
		this.goal = goal;
	}
	
}
