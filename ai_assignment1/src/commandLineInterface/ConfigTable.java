package commandLineInterface;

import java.util.Hashtable;

@SuppressWarnings("serial")
public class ConfigTable extends Hashtable< String,String>{

	public void parseConfExp(String exp) {
		String[] keyvalpair = exp.split("=");
		if(keyvalpair.length==2){
			put(keyvalpair[0],keyvalpair[1]);
		}
	}
	
}
