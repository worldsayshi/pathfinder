package commandLineInterface;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import search.Astar.GoalNotFoundException;
import search.Astar.NodeProcessedAgainException;
import dbBuilder.CityDataBase;
import dbBuilder.DistanceDataBase;
import dbBuilder.Reader;

public class Main {

//	static DistanceDataBase distdb = null;
//	static CityDataBase citydb = null;
	
	@SuppressWarnings("serial")
	public static class InvalidInputException extends Exception{
		public InvalidInputException(String invalidArgument) {
			super();
			this.invalidArgument = invalidArgument;
		}

		String invalidArgument;
	};
	
	private static void testInputValidity(String inp) throws InvalidInputException{
		if(inp.startsWith("-")){
			throw new InvalidInputException(inp);
		}
	}
	
	static String respath="../common/";
	static String respath_cmdline="../../common/";
	static String connectionspath="neighbours.txt";  // -city
	static String distancespath="distances.txt";     // -dist
	static String configpath="config.txt";           // -conf
	static String outputpath="solution.txt";	         // -out
	
	private static void processInputArgs(String[] args) throws InvalidInputException{
		
		for(int i=0; i < args.length; i++){
			if(args[i].equals("-city")){
				testInputValidity(args[i+1]);
				connectionspath=args[i+1];
				i++;
			}
			else if(args[i].equals("-dist")){
				testInputValidity(args[i+1]);
				distancespath=args[i+1];
				i++;
			}
			else if(args[i].equals("-out")){
				testInputValidity(args[i+1]);
				outputpath=args[i+1];
				i++;
			}
			else if(args[i].equals("-conf")){
				testInputValidity(args[i+1]);
				configpath=args[i+1];
				i++;
			}
			else if(args[i].equals("-cmd")){
				respath=respath_cmdline;
			}
		}
	}
	
	@SuppressWarnings("serial")
	static private class InValidConfigurationException extends Exception{
		public String detail;
		public InValidConfigurationException(String detail) {
			super();
			this.detail = detail;
		}
	}
	
	static private CityPathProblem setupProblem() throws InValidConfigurationException{
		CityDataBase citydb=Reader.pathToCityDb(respath+connectionspath);
		DistanceDataBase distdb=Reader.pathToDistanceDb(respath+distancespath);
		ConfigTable ct = Reader.pathToConfigTable(respath+configpath);
		String start=ct.get("start"); // TODO: These must be set
		String goal=ct.get("goal");
		if(start==null || goal==null){
			throw new InValidConfigurationException("start or goal not specified");
		}
		CityPathProblem problem = new CityPathProblem(citydb, distdb,
				start, goal);
		return problem;
	}
	
	/*
	 * 
	 */
	public static void main(String[] args) {
		
		try{
			processInputArgs(args);
		}
		catch (InvalidInputException e){
			System.out.println("Invalid input: " + e.invalidArgument);
		}
		
		//Load the databases from the text files
		CityPathProblem problem;
		try {
			problem = setupProblem();
		} catch (InValidConfigurationException e1) {
			System.out.println("Error in configuration: "+e1.detail);
			return;
		}
		
		ArrayList<String> solution=null;
		try {
			solution = search.Astar.findShortestPath(problem);
		} catch (GoalNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NodeProcessedAgainException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writeSolutionToFile(solution);
	  System.out.println("Solution:");
	  for(String city:solution){
		  System.out.println(city); 
	  }								 
	}

	private static void writeSolutionToFile(ArrayList<String> solution) {
		BufferedWriter out=null;
        try {
			out = new BufferedWriter(new FileWriter(respath+outputpath));
			for(String city:solution){
				out.write(city+"\n");
			}
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}