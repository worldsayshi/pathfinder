package dbBuilder;

import java.util.ArrayList;
//import java.util.Iterator;

/*
 * TODO: This could easily be reworked to a relational database with non-pre-defined number of fields.
 * It could also be merged with DistanceDataBase
 */
public class CityDataBase {
	
	@SuppressWarnings("serial")
	public class EntryNotFoundException extends Exception{}

	@SuppressWarnings("serial")
	public class MatchingException extends Exception{}
	
	public class Connection{
		String city1;
		String city2;
		int cost;
		
		public Connection(String[] entry){
			city1 = entry[0];
			city2 = entry[1];
			cost =  Integer.parseInt(entry[2]);
		}
		
		public int getCost(){
			return cost;
		}

		public String getOtherEnd(String name) throws MatchingException {
			if(name.equals(city1)) return city2;
			if(name.equals(city2)) return city1;
			throw new MatchingException();
		}
	}
	public ArrayList<Connection> getNeighbourConnections(String City){
		ArrayList<Connection> neighbours = new ArrayList<Connection>();
		for (Connection entry:connections){
			if(entry.city1.equals(City) || entry.city2.equals(City)){
				neighbours.add(entry);
			}
		}
		return neighbours;
	}
	
	public int getConnectionVal(String city1, String city2) throws EntryNotFoundException{
		for(Connection entry: connections){
			if(entryEquality(city1,city2,entry)){
				return entry.cost;
			}
		}
		throw new EntryNotFoundException();
	}

	public static boolean entryEquality(String city1, String city2,
			Connection entry) {
		if (city1.equals(entry.city2) && city2.equals(entry.city1)
				|| city1.equals(entry.city1) && city2.equals(entry.city2)) {
			return true;
		}
		return false;
	}
	
	public static boolean checkEntryEquality(Connection entry1, Connection entry2) {
		if(
		   entry1.city1.equals(entry2.city2) && 
		   entry1.city1.equals(entry2.city2) ||
		   entry1.city1.equals(entry1.city2) &&
		   entry1.city2.equals(entry1.city2) 
		   )
		{
			return true;
		}
		return false;
	}
	
	ArrayList<Connection> connections = new ArrayList<Connection>();
	
	public void insert(Connection connection){
		connections.add(connection);
	}

	public void insert(String currline) {
		String[] sEntry = currline.split(",");
		Connection entry = new Connection(sEntry);
		insert(entry);
	}

}
