package dbBuilder;

import java.util.ArrayList;

public class DistanceDataBase {
	ArrayList<String> Cities = new ArrayList<String>();
	ArrayList<ArrayList<Integer>> values = new ArrayList<ArrayList<Integer>>();
	
	@SuppressWarnings("serial")
	public static class CouldNotFindNodeException extends Exception{}
	
	public void insert(String line) {
		String[] sEntry = line.split(",");
		Cities.add(sEntry[0]);
		ArrayList<Integer> newMatrRow = new ArrayList<Integer>(); 
		for(int i=1;i<sEntry.length;i++){
			newMatrRow.add(i-1,Integer.parseInt(sEntry[i]));
		}
		values.add(newMatrRow);
	}
	
	public int getDistance(String from, String to) throws CouldNotFindNodeException{
		int i=0, j=0;
		
		for( i=0;i<Cities.size();i++){
			if(Cities.get(i).equals(from)){
				break;
			}
		}
		for( j=0;j<Cities.size();j++){
			if(Cities.get(j).equals(to)){
				break;
			}
		}
		if(i==Cities.size() || j==Cities.size()){
			throw new CouldNotFindNodeException();
		}
		return values.get(i).get(j);
	}
	
}
