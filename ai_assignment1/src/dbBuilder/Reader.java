package dbBuilder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import commandLineInterface.ConfigTable;

public class Reader {

	// Assuming valid file, with no duplicates, as input
	public static DistanceDataBase pathToDistanceDb(String pathToCityDb) {
		BufferedReader br = pathToBufferedReader(pathToCityDb);
		DistanceDataBase distdb = new DistanceDataBase();
		String currline;
		
		try {
			while ((currline = br.readLine()) != null) {
				distdb.insert(currline);
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		return distdb;
	}
		
	// Assuming valid file, with no duplicates, as input
	public static CityDataBase pathToCityDb(String pathToCityDb) {

		BufferedReader br = pathToBufferedReader(pathToCityDb);
		CityDataBase citydb = new CityDataBase();
		String currline;
		
		try {
			while ((currline = br.readLine()) != null) {
				citydb.insert(currline);
			}
		} 
		catch (IOException e2) {
			e2.printStackTrace();
		}
		return citydb;
	}
	
	public static ConfigTable pathToConfigTable(String path){
		ConfigTable ct = new ConfigTable();
		BufferedReader br = pathToBufferedReader(path);
		String currline=null;
		try {
			while ((currline = br.readLine()) != null) {
				ct.parseConfExp(currline);
			}
		} 
		catch (IOException e2) {
			e2.printStackTrace();
		}
		return ct;
	}
	
	private static BufferedReader pathToBufferedReader(String path){
		// file to file io stream
		FileInputStream fileiostream = null;
		try {
			fileiostream = new FileInputStream(path);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// file stream to buffered reader
		if (fileiostream == null) {return null;}
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(fileiostream));

		// buffered reader to database
		return br;
	}

}
